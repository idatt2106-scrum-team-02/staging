#! /usr/bin/sh

command -v git >/dev/null 2>&1 || { echo >&2 "Git not found"; local should_exit=1; }
command -v docker >/dev/null 2>&1 || { echo >&2 "Docker not found"; local should_exit=1; }

if [ $should_exit ]; then
exit 1;
fi

if [ ! -d frontend ]; then
    echo "Cloning frontend repository..."
    git clone git@gitlab.stud.idi.ntnu.no:idatt2106-scrum-team-02/frontend.git

    if [ ! $% ]; then
        echo "Unable to clone frontend repository using SSH";
    fi
else
    echo "Frontend repository already cloned, skipping"
fi

if [ ! -d backend ]; then
    echo "Cloning backend repository..."
    git clone git@gitlab.stud.idi.ntnu.no:idatt2106-scrum-team-02/backend.git

    if [ ! $% ]; then
        echo "Unable to clone backend repository using SSH";
    fi
else
    echo "Backend repository already cloned, skipping"
fi

echo "Done cloning repositories! Building service containers..."

docker compose build

echo "Setup complete! To start services, run 'docker compose up'";
echo "First run may fail due to a database creation race condition, if this happens press CTRL-C to stop the service, and re-run them."
echo "Once the services are started, the app is available on http://localhost:8888"