# Staging

This repo exists to streamline deployment of our app, consolidating the building and orchestration of all services/system components to a single Docker Compose configuration. The build steps of all components are performed within separate build containers that bundle all build-time dependencies, meaning the only requirements to run the project are Docker (with Compose) and Git (optional, to clone the frontend and backend repositories programmatically).

## Automatic setup
A `setup.sh` script is provided for convenience, this script can be run in Unix-like environments. It requires a POSIX shell such as bash or zsh, which should be provided by default on all Linux distributions and MacOS. This script will clone the frontend and backend repositories and run the build steps of all services, so that they are ready to start using `docker compose up`. 

## Manual setup
If you are not in a position to use this script, you can manually clone the two repositories for [frontend](https://gitlab.stud.idi.ntnu.no/idatt2106-scrum-team-02/frontend) and [backend](https://gitlab.stud.idi.ntnu.no/idatt2106-scrum-team-02/backend) and place them inside this repository alongside the `setup.sh` and `docker-compose.yml` files. Once these are in place, run `docker compose build` to run the build steps of all services, and `docker compose up` to start the services.

Note! There is a known potential race condition on first launch of the application where the database service will be busy doing first-launch setup while the Java Spring backend service is launching. The Java backend service attempts to connect to the database service as part of its launch process, which fails if the database itself has not completed its first-launch setup yet. If this occurs and the Java Spring backend service fails to start, relaunching the services with Docker Compose fixes the issue.
